package com.awitd.tailor;

public class Customers {

	private int Id;
	private String Name;
	private String Address;
	private String Phone;

	public Customers() {
		// TODO Auto-generated constructor stub
	}

	public Customers(int Id, String Name, String Address, String Phone) {
		this.Id = Id;
		this.Name = Name;
		this.Address = Address;
		this.Phone = Phone;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

}
