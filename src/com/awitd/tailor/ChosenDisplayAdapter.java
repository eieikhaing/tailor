package com.awitd.tailor;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ChosenDisplayAdapter extends BaseAdapter {

	Context mContext;
	LayoutInflater inflater;
	private DBHelper myDBHelper;
	private SQLiteDatabase mydb;
	private AlertDialog.Builder builder;
	private List<Clothes> ChosenClothesList = new ArrayList<Clothes>();
	private ArrayList<Clothes> arraylist;
	String status;

	public ChosenDisplayAdapter(Context context, List<Clothes> clothes_List,
			String status) {

		mContext = context;
		this.ChosenClothesList = clothes_List;
		inflater = LayoutInflater.from(mContext);
		this.arraylist = new ArrayList<Clothes>();
		this.arraylist.addAll(clothes_List);
		this.status = status;

	}

	public class ViewHolder {
		TextView name;
	}

	@Override
	public int getCount() {
		return ChosenClothesList.size();
	}

	@Override
	public Clothes getItem(int position) {
		return ChosenClothesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup parent) {

		final ViewHolder holder;
		if (view == null) {

			holder = new ViewHolder();
			view = inflater.inflate(R.layout.chosen_list1, null);

			holder.name = (TextView) view.findViewById(R.id.txtChosen);

			view.setTag(holder);

		} else {

			holder = (ViewHolder) view.getTag();

		}
		holder.name.setText(ChosenClothesList.get(position).getmName() + "");

		mydb = (new DBHelper(mContext)).getWritableDatabase();

		view.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				if (status == "R") {
					builder = new AlertDialog.Builder(mContext);
					builder.setTitle("Delete"
							+ ChosenClothesList.get(position).getmName());
					builder.setMessage("Do you want to delete?");
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@SuppressWarnings("static-access")
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

									mydb.delete(
											myDBHelper.TABLE_NAME1,
											myDBHelper.M_ID
													+ "="
													+ ChosenClothesList.get(
															position).getmId(),
											null);

									dialog.cancel();
									Intent intent = new Intent(mContext,
											ChosenClothesList.class);

									mContext.startActivity(intent);

								}

							});
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.cancel();
								}
							});
					AlertDialog alert = builder.create();
					alert.show();

				}
				return true;
			}

		});

		view.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (status == "R") {

					Intent intent = new Intent(mContext, Measurement.class);

					intent.putExtra("ID", ChosenClothesList.get(position)
							.getmId());
					intent.putExtra("Name", ChosenClothesList.get(position)
							.getmName());
					intent.putExtra("Neck", ChosenClothesList.get(position)
							.getmNeck());
					intent.putExtra("Shoulder", ChosenClothesList.get(position)
							.getmShoulder());
					intent.putExtra("Jine", ChosenClothesList.get(position)
							.getmJine());
					intent.putExtra("Chest", ChosenClothesList.get(position)
							.getmChest());
					intent.putExtra("Stomash", ChosenClothesList.get(position)
							.getmStomash());
					intent.putExtra("Hip", ChosenClothesList.get(position)
							.getmHip());
					intent.putExtra("Wrist", ChosenClothesList.get(position)
							.getmWrist());
					intent.putExtra("Sleeve", ChosenClothesList.get(position)
							.getmSleeve());
					intent.putExtra("Bicep", ChosenClothesList.get(position)
							.getmBicep());
					intent.putExtra("Length", ChosenClothesList.get(position)
							.getmLength());
					intent.putExtra("Waist", ChosenClothesList.get(position)
							.getmWaist());
					intent.putExtra("Outseam", ChosenClothesList.get(position)
							.getmOutseam());
					intent.putExtra("Date", ChosenClothesList.get(position)
							.getmDate());
					intent.putExtra("Check", ChosenClothesList.get(position)
							.getmCheck());
					intent.putExtra("Ready", ChosenClothesList.get(position)
							.getmReady());
					intent.putExtra("Remark", ChosenClothesList.get(position)
							.getmRemark());
					intent.putExtra("chosenupdate", true);
					intent.putExtra("path", ChosenClothesList.get(position)
							.getmPath());
					mContext.startActivity(intent);
				}

			}
		});

		return view;
	}

	// Filter Class
	public void filter(String charText) {

		charText = charText.toLowerCase(Locale.getDefault());
		ChosenClothesList.clear();
		if (charText.length() == 0) {

			ChosenClothesList.addAll(arraylist);
		} else {
			for (Clothes tp : arraylist) {
				if (tp.getmName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					ChosenClothesList.add(tp);
				}
			}
		}
		notifyDataSetChanged();
	}

}
