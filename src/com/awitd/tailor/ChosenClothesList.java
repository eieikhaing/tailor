package com.awitd.tailor;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;

@SuppressLint("NewApi")
public class ChosenClothesList extends Activity {

	EditText txtSearchClothes;
	ListView listClothess;
	ChosenDisplayAdapter adapter;
	private DBHelper dbHelper;
	ArrayList<Clothes> Chosen_List = new ArrayList<Clothes>();
	private ActionBar actionbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chosen_clothes_list);

		actionbar = getActionBar();
		actionbar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#f06292")));
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setDisplayHomeAsUpEnabled(true);

		dbHelper = new DBHelper(this);

		txtSearchClothes = (EditText) findViewById(R.id.searchname);
		listClothess = (ListView) findViewById(R.id.chosenList);
		List<Clothes> ClothObject_List = dbHelper.getChosenClothesList();

		for (Clothes cn : ClothObject_List) {
			cn.getmId();
			cn.getmName();
			cn.getmNeck();
			cn.getmShoulder();
			cn.getmJine();
			cn.getmChest();
			cn.getmStomash();
			cn.getmHip();
			cn.getmWrist();
			cn.getmSleeve();
			cn.getmBicep();
			cn.getmLength();
			cn.getmWaist();
			cn.getmOutseam();
			cn.getmDate();
			cn.getmCheck();
			cn.getmReady();
			cn.getmRemark();
			cn.getmPath();
			Chosen_List.add(cn);

		}

		adapter = new ChosenDisplayAdapter(this, Chosen_List, "R");

		listClothess.setAdapter(adapter);

		txtSearchClothes.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				String text = txtSearchClothes.getText().toString()
						.toLowerCase(Locale.getDefault());
				Log.e("Search", text);
				adapter.filter(text);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();

	}
}
