package com.awitd.tailor;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

@SuppressLint("NewApi")
public class DisplayCustomer extends Activity {
	private ActionBar actionbar;
	private DBHelper dbHelper;
	DisplayAdapter adapter;
	Cursor cursor;
	ArrayList<Customers> Customer_List = new ArrayList<Customers>();
	private ListView customerList;
	EditText edtSearchName;
	ImageButton btnAdd;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.display_customer);
		actionbar = getActionBar();
		actionbar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#f06292")));
		actionbar.setDisplayShowTitleEnabled(true);
		actionbar.setDisplayHomeAsUpEnabled(true);

		(new DBHelper(this)).getWritableDatabase();
		customerList = (ListView) findViewById(R.id.List);
		dbHelper = new DBHelper(this);

		edtSearchName = (EditText) findViewById(R.id.edtnamesearch);
		btnAdd = (ImageButton) findViewById(R.id.btnAdd);
		List<Customers> CustomerObject_List = dbHelper.getAllCustomer();
		for (Customers cn : CustomerObject_List) {

			cn.getId();
			cn.getName();
			cn.getAddress();
			cn.getPhone();

			Customer_List.add(cn);

		}

		adapter = new DisplayAdapter(this, Customer_List, "R");

		customerList.setAdapter(adapter);

		edtSearchName.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				String text = edtSearchName.getText().toString()
						.toLowerCase(Locale.getDefault());
				Log.e("Search", text);
				adapter.filter(text);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
			}
		});

		btnAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),
						AddCustomer.class);
				i.putExtra("update", false);
				startActivity(i);
			}
		});

	}

	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();

	}
}
