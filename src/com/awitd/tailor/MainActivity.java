package com.awitd.tailor;

import java.util.ArrayList;
import android.net.Uri;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements OnClickListener {

	private static final String URI = "https://play.google.com/store/search?q=awitdj";
	private ActionBar actionbar;
	Button btn1, btn2, btn3, btn4, btn5;
	String name;
	ArrayList<String> namelist = new ArrayList<String>();
	ArrayList<Integer> CID_list = new ArrayList<Integer>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		new DBHelper(this);
		actionbar = getActionBar();
		actionbar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#f06292")));
		actionbar.setDisplayShowTitleEnabled(true);

		btn1 = (Button) findViewById(R.id.btn1);
		btn2 = (Button) findViewById(R.id.btn2);
		btn3 = (Button) findViewById(R.id.btn3);
		btn4 = (Button) findViewById(R.id.btn4);
		btn5 = (Button) findViewById(R.id.btn5);

		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
		btn4.setOnClickListener(this);
		btn5.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Take appropriate action for each action item click
		int id = item.getItemId();

		if (id == R.id.about) {
			Intent intent1 = new Intent(MainActivity.this, AboutUs.class);
			startActivity(intent1);

		} else if (id == R.id.action_settings) {
			Uri uri = Uri.parse(URI);
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			startActivity(intent);
		} else {
			System.exit(id);
		}
		return true;

	}

	@Override
	public void onClick(View v) {
		Button btnObj = (Button) v;
		switch (btnObj.getId()) {
		case R.id.btn1:
			Intent intent1 = new Intent(MainActivity.this,
					DisplayCustomer.class);
			startActivity(intent1);
			break;

		case R.id.btn2:
			Intent intent2 = new Intent(MainActivity.this, Measurement.class);
			intent2.putExtra("update", false);
			startActivity(intent2);

			break;

		case R.id.btn3:
			Intent intent3 = new Intent(MainActivity.this,
					SewingClothesList.class);

			startActivity(intent3);
			break;

		case R.id.btn4:
			Intent intent4 = new Intent(MainActivity.this,
					ReadyClothesList.class);
			startActivity(intent4);
			break;

		case R.id.btn5:
			Intent intent5 = new Intent(MainActivity.this,
					ChosenClothesList.class);
			startActivity(intent5);
			break;
		default:
			break;
		}
		// TODO Auto-generated method stub

	}

}
