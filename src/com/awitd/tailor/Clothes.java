package com.awitd.tailor;

public class Clothes {

	private int mId;
	private String mName;
	private String mNeck;
	private String mShoulder;
	private String mJine;
	private String mChest;
	private String mStomash;
	private String mHip;
	private String mWrist;
	private String mSleeve;
	private String mBicep;
	private String mLength;
	private String mWaist;
	private String mOutseam;
	private String mDate;
	private String mCheck;
	private String mReady;
	private String mRemark;
	private String mPath;

	public Clothes() {
	}

	public Clothes(int mId, String mName, String mNeck, String mShoulder,
			String mJine, String mChest, String mStomash, String mHip,
			String mWrist, String mSleeve, String mBicep, String mLength,
			String mWaist, String mOutseam, String mDate, String mCheck,
			String mReady, String mRemark, String imgpath) {
		super();
		this.mId = mId;
		this.mName = mName;
		this.mNeck = mNeck;
		this.mShoulder = mShoulder;
		this.mJine = mJine;
		this.mChest = mChest;
		this.mStomash = mStomash;
		this.mHip = mHip;
		this.mWrist = mWrist;
		this.mSleeve = mSleeve;
		this.mBicep = mBicep;
		this.mLength = mLength;
		this.mWaist = mWaist;
		this.mOutseam = mOutseam;
		this.mDate = mDate;
		this.mCheck = mCheck;
		this.mReady = mReady;
		this.mRemark = mRemark;

		setmPath(imgpath);

	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getmNeck() {
		return mNeck;
	}

	public void setmNeck(String mNeck) {
		this.mNeck = mNeck;
	}

	public String getmShoulder() {
		return mShoulder;
	}

	public void setmShoulder(String mShoulder) {
		this.mShoulder = mShoulder;
	}

	public String getmJine() {
		return mJine;
	}

	public void setmJine(String mJine) {
		this.mJine = mJine;
	}

	public String getmChest() {
		return mChest;
	}

	public void setmChest(String mChest) {
		this.mChest = mChest;
	}

	public String getmStomash() {
		return mStomash;
	}

	public void setmStomash(String mStomash) {
		this.mStomash = mStomash;
	}

	public String getmHip() {
		return mHip;
	}

	public void setmHip(String mHip) {
		this.mHip = mHip;
	}

	public String getmWrist() {
		return mWrist;
	}

	public void setmWrist(String mWrist) {
		this.mWrist = mWrist;
	}

	public String getmSleeve() {
		return mSleeve;
	}

	public void setmSleeve(String mSleeve) {
		this.mSleeve = mSleeve;
	}

	public String getmBicep() {
		return mBicep;
	}

	public void setmBicep(String mBicep) {
		this.mBicep = mBicep;
	}

	public String getmLength() {
		return mLength;
	}

	public void setmLength(String mLength) {
		this.mLength = mLength;
	}

	public String getmWaist() {
		return mWaist;
	}

	public void setmWaist(String mWaist) {
		this.mWaist = mWaist;
	}

	public String getmOutseam() {
		return mOutseam;
	}

	public void setmOutseam(String mOutseam) {
		this.mOutseam = mOutseam;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getmCheck() {
		return mCheck;
	}

	public void setmCheck(String mCheck) {
		this.mCheck = mCheck;
	}

	public String getmReady() {
		return mReady;
	}

	public void setmReady(String mReady) {
		this.mReady = mReady;
	}

	public String getmRemark() {
		return mRemark;
	}

	public void setmRemark(String mRemark) {
		this.mRemark = mRemark;
	}

	public String getmPath() {
		return mPath;
	}

	public void setmPath(String mPath) {
		this.mPath = mPath;
	}

}
