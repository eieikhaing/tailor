package com.awitd.tailor;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	static String DATABASE_NAME = "db";
	public static final String TABLE_NAME = "customer";
	public static final String KEY_NAME = "name";
	public static final String KEY_ADDRESS = "address";
	public static final String KEY_PHONE = "phone";
	public static final String KEY_ID = "id";

	public static final String TABLE_NAME1 = "measurement";
	public static final String M_ID = "mid";
	public static final String M_NAME = "mname";
	public static final String M_NECK = "neck";
	public static final String M_SHOULDER = "shoulder";
	public static final String M_JINE = "jine";
	public static final String M_CHEST = "chest";
	public static final String M_STOMASH = "stomash";
	public static final String M_HIP = "hip";
	public static final String M_WRIST = "wrist";
	public static final String M_SLEEVE = "sleeve";
	public static final String M_BICEP = "bicep";
	public static final String M_LENGTH = "length";
	public static final String M_WAIST = "waist";
	public static final String M_OUTSEAM = "outseam";
	public static final String M_DATE = "date";
	public static final String M_CHECK = "checkbox";
	public static final String M_READY = "ready";
	public static final String M_REMARK = "remark";
	public static final String M_PICPATH = "path";

	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	public ArrayList<String> getCustomerName() {

		ArrayList<String> nameList = new ArrayList<String>();
		SQLiteDatabase db = this.getWritableDatabase();
		String query = "SELECT * FROM customer";
		Cursor cursor = db.rawQuery(query, null);
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			nameList.add(cursor.getString(cursor.getColumnIndex(KEY_NAME)));

			cursor.moveToNext();
		}

		return nameList;
	}

	public List<Customers> getAllCustomer() {

		List<Customers> customer_list = new ArrayList<Customers>();

		SQLiteDatabase db = this.getWritableDatabase();

		String selectQuery = "SELECT * FROM customer ";

		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				Customers customer = new Customers();

				customer.setId(Integer.parseInt(cursor.getString(0)));

				customer.setName(cursor.getString(1));
				customer.setAddress(cursor.getString(2));
				customer.setPhone(cursor.getString(3));

				// Adding cloth to list
				customer_list.add(customer);
			} while (cursor.moveToNext());
		}

		return customer_list;
	}

	public List<Clothes> getAllClothes() {

		List<Clothes> clothes_list = new ArrayList<Clothes>();

		SQLiteDatabase db = this.getWritableDatabase();

		String selectQuery = "SELECT * FROM measurement  WHERE checkbox='no' AND ready ='no' ORDER BY date  ";

		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				Clothes cloth = new Clothes();

				cloth.setmId(Integer.parseInt(cursor.getString(0)));

				cloth.setmName(cursor.getString(1));
				cloth.setmNeck(cursor.getString(2));
				cloth.setmShoulder(cursor.getString(3));
				cloth.setmJine(cursor.getString(4));
				cloth.setmChest(cursor.getString(5));
				cloth.setmStomash(cursor.getString(6));
				cloth.setmHip(cursor.getString(7));
				cloth.setmWrist(cursor.getString(8));
				cloth.setmSleeve(cursor.getString(9));
				cloth.setmBicep(cursor.getString(10));
				cloth.setmLength(cursor.getString(11));
				cloth.setmWaist(cursor.getString(12));
				cloth.setmOutseam(cursor.getString(13));
				cloth.setmDate(cursor.getString(14));
				cloth.setmCheck(cursor.getString(15));
				cloth.setmReady(cursor.getString(16));
				cloth.setmRemark(cursor.getString(17));
				cloth.setmPath(cursor.getString(18));
				// Adding cloth to list
				clothes_list.add(cloth);
			} while (cursor.moveToNext());
		}

		return clothes_list;
	}

	public List<Clothes> getReadyClothesList() {

		List<Clothes> readyClothes_list = new ArrayList<Clothes>();

		SQLiteDatabase db = this.getWritableDatabase();

		String selectQuery = "SELECT * FROM measurement  WHERE ready='yes' AND checkbox='no' ORDER BY date ";

		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				Clothes cloth = new Clothes();

				cloth.setmId(Integer.parseInt(cursor.getString(0)));
				cloth.setmName(cursor.getString(1));
				cloth.setmNeck(cursor.getString(2));
				cloth.setmShoulder(cursor.getString(3));
				cloth.setmJine(cursor.getString(4));
				cloth.setmChest(cursor.getString(5));
				cloth.setmStomash(cursor.getString(6));
				cloth.setmHip(cursor.getString(7));
				cloth.setmWrist(cursor.getString(8));
				cloth.setmSleeve(cursor.getString(9));
				cloth.setmBicep(cursor.getString(10));
				cloth.setmLength(cursor.getString(11));
				cloth.setmWaist(cursor.getString(12));
				cloth.setmOutseam(cursor.getString(13));
				cloth.setmDate(cursor.getString(14));
				cloth.setmCheck(cursor.getString(15));
				cloth.setmReady(cursor.getString(16));
				cloth.setmRemark(cursor.getString(17));
				cloth.setmPath(cursor.getString(18));
				readyClothes_list.add(cloth);

			} while (cursor.moveToNext());
		}
		return readyClothes_list;
	}

	public List<Clothes> getChosenClothesList() {

		List<Clothes> chosenClothes_list = new ArrayList<Clothes>();

		SQLiteDatabase db = this.getWritableDatabase();

		String selectQuery = "SELECT * FROM measurement  WHERE checkbox='yes' ORDER BY date";

		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				Clothes cloth = new Clothes();

				cloth.setmId(Integer.parseInt(cursor.getString(0)));

				cloth.setmName(cursor.getString(1));
				cloth.setmNeck(cursor.getString(2));
				cloth.setmShoulder(cursor.getString(3));
				cloth.setmJine(cursor.getString(4));
				cloth.setmChest(cursor.getString(5));
				cloth.setmStomash(cursor.getString(6));
				cloth.setmHip(cursor.getString(7));
				cloth.setmWrist(cursor.getString(8));
				cloth.setmSleeve(cursor.getString(9));
				cloth.setmBicep(cursor.getString(10));
				cloth.setmLength(cursor.getString(11));
				cloth.setmWaist(cursor.getString(12));
				cloth.setmOutseam(cursor.getString(13));
				cloth.setmDate(cursor.getString(14));
				cloth.setmCheck(cursor.getString(15));
				cloth.setmReady(cursor.getString(16));
				cloth.setmRemark(cursor.getString(17));
				cloth.setmPath(cursor.getString(18));
				chosenClothes_list.add(cloth);
			} while (cursor.moveToNext());
		}
		return chosenClothes_list;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE IF NOT EXISTS "
				+ TABLE_NAME
				+ " (id Integer PRIMARY KEY autoincrement,name TEXT,address VARCHAR,phone VARCHAR);");
		db.execSQL("CREATE TABLE IF NOT EXISTS "
				+ TABLE_NAME1
				+ " (mid Integer PRIMARY KEY autoincrement,mname TEXT,neck VARCHAR,shoulder VARCHAR,jine VARCHAR,chest VARCHAR,stomash VARCHAR,hip VARCHAR,wrist VARCHAR,sleeve VARCHAR,bicep VARCHAR,length VARCHAR,waist VARCHAR,outseam VARCHAR,date DATE,checkbox TEXT,ready TEXT,remark VARCHAR,path TEXT);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME1);
		onCreate(db);

	}

}
