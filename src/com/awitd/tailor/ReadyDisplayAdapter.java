package com.awitd.tailor;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReadyDisplayAdapter extends BaseAdapter {
	Context mContext;
	LayoutInflater inflater;
	private DBHelper myDBHelper;
	private SQLiteDatabase mydb;
	private AlertDialog.Builder builder;

	private List<Clothes> ReadyClothesList = new ArrayList<Clothes>();
	private ArrayList<Clothes> arraylist;

	String status;

	public ReadyDisplayAdapter(Context context, List<Clothes> clothes_List,
			String status) {
		mContext = context;
		this.ReadyClothesList = clothes_List;
		inflater = LayoutInflater.from(mContext);
		this.arraylist = new ArrayList<Clothes>();
		this.arraylist.addAll(clothes_List);
		this.status = status;
	}

	public class ViewHolder {
		TextView name;
	}

	@Override
	public int getCount() {
		return ReadyClothesList.size();
	}

	@Override
	public Clothes getItem(int position) {
		return ReadyClothesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.ready_list1, null);

			holder.name = (TextView) view.findViewById(R.id.txtReady);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		holder.name.setText(ReadyClothesList.get(position).getmName() + "");

		mydb = (new DBHelper(mContext)).getWritableDatabase();

		view.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				if (status == "R") {
					builder = new AlertDialog.Builder(mContext);
					builder.setTitle("Delete"
							+ ReadyClothesList.get(position).getmName());
					builder.setMessage("Do you want to delete?");
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@SuppressWarnings("static-access")
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

									mydb.delete(
											myDBHelper.TABLE_NAME1,
											myDBHelper.M_ID
													+ "="
													+ ReadyClothesList.get(
															position).getmId(),
											null);

									dialog.cancel();
									Intent intent = new Intent(mContext,
											ReadyClothesList.class);
								
									mContext.startActivity(intent);

								}

							});
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.cancel();
								}
							});
					AlertDialog alert = builder.create();
					alert.show();

				}
				return true;
			}

		});
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (status == "R") {

					Intent intent = new Intent(mContext, Measurement.class);

					intent.putExtra("ID", ReadyClothesList.get(position)
							.getmId());
					intent.putExtra("Name", ReadyClothesList.get(position)
							.getmName());
					intent.putExtra("Neck", ReadyClothesList.get(position)
							.getmNeck());
					intent.putExtra("Shoulder", ReadyClothesList.get(position)
							.getmShoulder());
					intent.putExtra("Jine", ReadyClothesList.get(position)
							.getmJine());
					intent.putExtra("Chest", ReadyClothesList.get(position)
							.getmChest());
					intent.putExtra("Stomash", ReadyClothesList.get(position)
							.getmStomash());
					intent.putExtra("Hip", ReadyClothesList.get(position)
							.getmHip());
					intent.putExtra("Wrist", ReadyClothesList.get(position)
							.getmWrist());
					intent.putExtra("Sleeve", ReadyClothesList.get(position)
							.getmSleeve());
					intent.putExtra("Bicep", ReadyClothesList.get(position)
							.getmBicep());
					intent.putExtra("Length", ReadyClothesList.get(position)
							.getmLength());
					intent.putExtra("Waist", ReadyClothesList.get(position)
							.getmWaist());
					intent.putExtra("Outseam", ReadyClothesList.get(position)
							.getmOutseam());
					intent.putExtra("Date", ReadyClothesList.get(position)
							.getmDate());
					intent.putExtra("Check", ReadyClothesList.get(position)
							.getmCheck());

					intent.putExtra("Ready", ReadyClothesList.get(position)
							.getmReady());
					intent.putExtra("Remark", ReadyClothesList.get(position)
							.getmRemark());
					intent.putExtra("readyupdate", true);
					intent.putExtra("path", ReadyClothesList.get(position)
							.getmPath());
					mContext.startActivity(intent);
				}

			}
		});

		return view;
	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		ReadyClothesList.clear();
		if (charText.length() == 0) {
			ReadyClothesList.addAll(arraylist);
		} else {
			for (Clothes tp : arraylist) {
				if (tp.getmName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					ReadyClothesList.add(tp);
				}
			}
		}
		notifyDataSetChanged();
	}

}
