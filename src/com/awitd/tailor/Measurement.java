package com.awitd.tailor;

import java.util.ArrayList;
import java.util.Calendar;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.widget.DatePicker;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class Measurement extends Activity {
	private ActionBar actionbar;
	private DBHelper dbHelper;
	private SQLiteDatabase db;
	private boolean isUpdated, isreadyUpdated, ischosenUpdated;
	private String checked = "no";
	private String ready = "no";
	Button btnSave, btncalander, btnchoosephoto;
	String name, neck, shoulder, jine, chest, stomash, hip, wrist, sleeve,
			bicep, length, waist, outseam, remark, date, responsecheck,
			readycheck;
	private int myear;
    private int mmonth;
    private int mday;

    static final int DATE_DIALOG_ID = 999;
	private Dialog imgdialog;
	ImageView img;
	int id;

	EditText edtname, edtneck, edtshoulder, edtjine, edtchest, edtstomash,
			edthip, edtwrist, edtsleeve, edtbicep, edtlength, edtwaist,
			edtoutseam, edtremark;
	AutoCompleteTextView autocompletetxtView;
	CheckBox chkFinish, chkReady;
	ImageView imgphoto;
	TextView txtdate;
	DisplayAdapter adapter;

	private static final int SELECT_PICTURE = 1;

	private String selectedImagePath;
	public static Uri selectedImageUri;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void onCreate(Bundle save) {
		super.onCreate(save);

		setContentView(R.layout.measurement);
		dbHelper = new DBHelper(this);

		actionbar = getActionBar();
		actionbar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#f06292")));
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setDisplayHomeAsUpEnabled(true);

		autocompletetxtView = (AutoCompleteTextView) findViewById(R.id.autocompletxt);
		edtneck = (EditText) findViewById(R.id.neck);
		edtshoulder = (EditText) findViewById(R.id.shoulder);
		edtjine = (EditText) findViewById(R.id.jine);
		edtchest = (EditText) findViewById(R.id.full_chest);
		edtstomash = (EditText) findViewById(R.id.stomash);
		edthip = (EditText) findViewById(R.id.hip);
		edtwrist = (EditText) findViewById(R.id.wrist);
		edtsleeve = (EditText) findViewById(R.id.sleeve);
		edtbicep = (EditText) findViewById(R.id.bicep);
		edtlength = (EditText) findViewById(R.id.length);
		chkFinish = (CheckBox) findViewById(R.id.chk_finish);
		chkReady = (CheckBox) findViewById(R.id.chk_ready);
		edtwaist = (EditText) findViewById(R.id.waist);
		edtoutseam = (EditText) findViewById(R.id.outseam);
		edtremark = (EditText) findViewById(R.id.edtnote);
		txtdate = (TextView) findViewById(R.id.edtDate);
		btnchoosephoto = (Button) findViewById(R.id.btnchoosephoto);
		imgphoto = (ImageView) findViewById(R.id.imgphoto);
		btncalander = (Button) findViewById(R.id.btncalender);
		btnSave = (Button) findViewById(R.id.measure_save);
		chkFinish.setVisibility(View.INVISIBLE);
		chkReady.setVisibility(View.INVISIBLE);
		isUpdated = getIntent().getExtras().getBoolean("sewingupdate");
		isreadyUpdated = getIntent().getExtras().getBoolean("readyupdate");
		ischosenUpdated = getIntent().getExtras().getBoolean("chosenupdate");

		final Calendar c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        
		ArrayList<String> nameList = dbHelper.getCustomerName();
		autocompletetxtView.setAdapter(new ArrayAdapter(this,
				android.R.layout.simple_list_item_single_choice, nameList));

		if (isUpdated) {
			id = getIntent().getExtras().getInt("ID");
			name = getIntent().getExtras().getString("Name");
			neck = getIntent().getExtras().getString("Neck");
			shoulder = getIntent().getExtras().getString("Shoulder");
			jine = getIntent().getExtras().getString("Jine");
			chest = getIntent().getExtras().getString("Chest");
			stomash = getIntent().getExtras().getString("Stomash");
			hip = getIntent().getExtras().getString("Hip");
			wrist = getIntent().getExtras().getString("Wrist");
			sleeve = getIntent().getExtras().getString("Sleeve");
			bicep = getIntent().getExtras().getString("Bicep");
			length = getIntent().getExtras().getString("Length");
			waist = getIntent().getExtras().getString("Waist");
			outseam = getIntent().getExtras().getString("Outseam");
			date = getIntent().getExtras().getString("Date");
			responsecheck = getIntent().getExtras().getString("Check");
			readycheck = getIntent().getExtras().getString("Ready");
			remark = getIntent().getExtras().getString("Remark");

			selectedImagePath = getIntent().getExtras().getString("path");

			chkFinish.setVisibility(View.VISIBLE);
			chkReady.setVisibility(View.VISIBLE);

			autocompletetxtView.setText(name);
			edtneck.setText(neck);
			edtshoulder.setText(shoulder);
			edtjine.setText(jine);
			edtchest.setText(chest);
			edtstomash.setText(stomash);
			edthip.setText(hip);
			edtwrist.setText(wrist);
			edtsleeve.setText(sleeve);
			edtbicep.setText(bicep);
			edtlength.setText(length);
			edtwaist.setText(waist);
			edtoutseam.setText(outseam);
			txtdate.setText(date);
			if (responsecheck.equalsIgnoreCase("yes")) {
				chkFinish.setChecked(true);
			} else {
				chkFinish.setChecked(false);
			}

			if (readycheck.equalsIgnoreCase("yes")) {
				chkReady.setChecked(true);
			} else {
				chkReady.setChecked(false);
			}
			edtremark.setText(remark);
			imgphoto.setImageDrawable(Drawable
					.createFromPath(selectedImagePath));
		}
		if (isreadyUpdated) {
			id = getIntent().getExtras().getInt("ID");
			name = getIntent().getExtras().getString("Name");
			neck = getIntent().getExtras().getString("Neck");
			shoulder = getIntent().getExtras().getString("Shoulder");
			jine = getIntent().getExtras().getString("Jine");
			chest = getIntent().getExtras().getString("Chest");
			stomash = getIntent().getExtras().getString("Stomash");
			hip = getIntent().getExtras().getString("Hip");
			wrist = getIntent().getExtras().getString("Wrist");
			sleeve = getIntent().getExtras().getString("Sleeve");
			bicep = getIntent().getExtras().getString("Bicep");
			length = getIntent().getExtras().getString("Length");
			waist = getIntent().getExtras().getString("Waist");
			outseam = getIntent().getExtras().getString("Outseam");
			date = getIntent().getExtras().getString("Date");
			responsecheck = getIntent().getExtras().getString("Check");
			readycheck = getIntent().getExtras().getString("Ready");
			remark = getIntent().getExtras().getString("Remark");

			selectedImagePath = getIntent().getExtras().getString("path");

			chkFinish.setVisibility(View.VISIBLE);
			chkReady.setVisibility(View.VISIBLE);

			autocompletetxtView.setText(name);
			edtneck.setText(neck);
			edtshoulder.setText(shoulder);
			edtjine.setText(jine);
			edtchest.setText(chest);
			edtstomash.setText(stomash);
			edthip.setText(hip);
			edtwrist.setText(wrist);
			edtsleeve.setText(sleeve);
			edtbicep.setText(bicep);
			edtlength.setText(length);
			edtwaist.setText(waist);
			edtoutseam.setText(outseam);
			txtdate.setText(date);
			if (responsecheck.equalsIgnoreCase("yes")) {
				chkFinish.setChecked(true);
			} else {
				chkFinish.setChecked(false);
			}

			if (readycheck.equalsIgnoreCase("yes")) {
				chkReady.setChecked(true);
			} else {
				chkReady.setChecked(false);
			}
			edtremark.setText(remark);

			imgphoto.setImageDrawable(Drawable
					.createFromPath(selectedImagePath));
		}
		if (ischosenUpdated) {
			id = getIntent().getExtras().getInt("ID");
			name = getIntent().getExtras().getString("Name");
			neck = getIntent().getExtras().getString("Neck");
			shoulder = getIntent().getExtras().getString("Shoulder");
			jine = getIntent().getExtras().getString("Jine");
			chest = getIntent().getExtras().getString("Chest");
			stomash = getIntent().getExtras().getString("Stomash");
			hip = getIntent().getExtras().getString("Hip");
			wrist = getIntent().getExtras().getString("Wrist");
			sleeve = getIntent().getExtras().getString("Sleeve");
			bicep = getIntent().getExtras().getString("Bicep");
			length = getIntent().getExtras().getString("Length");
			waist = getIntent().getExtras().getString("Waist");
			outseam = getIntent().getExtras().getString("Outseam");
			date = getIntent().getExtras().getString("Date");
			responsecheck = getIntent().getExtras().getString("Check");
			readycheck = getIntent().getExtras().getString("Ready");
			remark = getIntent().getExtras().getString("Remark");
			selectedImagePath = getIntent().getExtras().getString("path");

			chkFinish.setVisibility(View.VISIBLE);
			chkReady.setVisibility(View.VISIBLE);

			autocompletetxtView.setText(name);
			edtneck.setText(neck);
			edtshoulder.setText(shoulder);
			edtjine.setText(jine);
			edtchest.setText(chest);
			edtstomash.setText(stomash);
			edthip.setText(hip);
			edtwrist.setText(wrist);
			edtsleeve.setText(sleeve);
			edtbicep.setText(bicep);
			edtlength.setText(length);
			edtwaist.setText(waist);
			edtoutseam.setText(outseam);
			txtdate.setText(date);
			if (responsecheck.equalsIgnoreCase("yes")) {
				chkFinish.setChecked(true);
			} else {
				chkFinish.setChecked(false);
			}

			if (readycheck.equalsIgnoreCase("yes")) {
				chkReady.setChecked(true);
			} else {
				chkReady.setChecked(false);
			}
			edtremark.setText(remark);

			imgphoto.setImageDrawable(Drawable
					.createFromPath(selectedImagePath));

		}
		autocompletetxtView.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				// txtName.setText(autocompletetxtView.getText());

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// name = edtname.getText().toString().trim();
				name = autocompletetxtView.getText().toString().trim();
				neck = edtneck.getText().toString().trim();
				shoulder = edtshoulder.getText().toString().trim();
				jine = edtjine.getText().toString().trim();
				chest = edtchest.getText().toString().trim();
				stomash = edtstomash.getText().toString().trim();
				hip = edthip.getText().toString().trim();
				wrist = edtwrist.getText().toString().trim();
				sleeve = edtsleeve.getText().toString().trim();
				bicep = edtbicep.getText().toString().trim();
				length = edtlength.getText().toString().trim();
				waist = edtwaist.getText().toString().trim();
				outseam = edtoutseam.getText().toString().trim();
				remark = edtremark.getText().toString().trim();
				date = txtdate.getText().toString().trim();

				if (name.length() > 0 && date.length() > 0 && 
						 selectedImagePath != null) {
					saveMeasurement();
					Toast.makeText(Measurement.this,
							"Save the record successfully!", Toast.LENGTH_LONG)
							.show();
				} else {

					AlertDialog.Builder alertBuilder = new AlertDialog.Builder(
							Measurement.this);
					alertBuilder.setTitle("Invalid Data");
					alertBuilder
							.setMessage("အမည္၊ ရက္စြဲႏွင့္ ခ်ဳပ္ပုံ ထည့္ပါ။");
					alertBuilder.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.cancel();
								}

							});
					alertBuilder.create().show();
				}

			}
		});

	
		btncalander.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

              showDialog(DATE_DIALOG_ID);
            }

        });
		
	   
		imgphoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				LayoutInflater inflater = getLayoutInflater();
				imgdialog = new Dialog(v.getContext());
				View view = inflater.inflate(R.layout.pic_dialog, null);
				imgdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				imgdialog.setContentView(view);

				img = (ImageView) view.findViewById(R.id.imagedialog);
				img.setImageDrawable(Drawable.createFromPath(selectedImagePath));
				imgdialog.getWindow().setBackgroundDrawable(
						new ColorDrawable(android.graphics.Color.TRANSPARENT));
				imgdialog.show();

				// TODO Auto-generated method stub

			}
		});

		btnchoosephoto.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Select Picture"),
						SELECT_PICTURE);
			}
		});

	}


	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				selectedImageUri = data.getData();
				selectedImagePath = getPath(selectedImageUri);

				imgphoto.setImageDrawable(Drawable
						.createFromPath(selectedImagePath));

			}

		}
	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		@SuppressWarnings("deprecation")
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();

		return cursor.getString(column_index);
	}

	private void saveMeasurement() {

		if (chkFinish.isChecked()) {
			checked = "yes";
		} else {
			checked = "no";
		}
		if (chkReady.isChecked()) {
			ready = "yes";
		} else {
			ready = "no";
		}

		db = dbHelper.getWritableDatabase();
		ContentValues value = new ContentValues();
		value.put(DBHelper.M_NAME, name);
		value.put(DBHelper.M_NECK, neck);
		value.put(DBHelper.M_SHOULDER, shoulder);
		value.put(DBHelper.M_JINE, jine);
		value.put(DBHelper.M_CHEST, chest);
		value.put(DBHelper.M_STOMASH, stomash);
		value.put(DBHelper.M_HIP, hip);
		value.put(DBHelper.M_WRIST, wrist);
		value.put(DBHelper.M_SLEEVE, sleeve);
		value.put(DBHelper.M_BICEP, bicep);
		value.put(DBHelper.M_LENGTH, length);
		value.put(DBHelper.M_WAIST, waist);
		value.put(DBHelper.M_OUTSEAM, outseam);
		value.put(DBHelper.M_DATE, date);
		value.put(DBHelper.M_CHECK, checked);
		value.put(DBHelper.M_READY, ready);
		value.put(DBHelper.M_REMARK, remark);

		// value.put(DBHelper.M_PIC, inputData);
		value.put(DBHelper.M_PICPATH, selectedImagePath);
		if (isUpdated) {
			db.update(DBHelper.TABLE_NAME1, value, DBHelper.M_ID + "=" + id,
					null);

			Intent intent1 = new Intent(this, SewingClothesList.class);

			startActivity(intent1);
		} else if (isreadyUpdated) {
			db.update(DBHelper.TABLE_NAME1, value, DBHelper.M_ID + "=" + id,
					null);

			Intent intent2 = new Intent(this, ReadyClothesList.class);

			startActivity(intent2);
		}

		else if (ischosenUpdated) {
			db.update(DBHelper.TABLE_NAME1, value, DBHelper.M_ID + "=" + id,
					null);

			Intent intent3 = new Intent(this, ChosenClothesList.class);

			startActivity(intent3);
		}

		else {
			db.insert(DBHelper.TABLE_NAME1, null, value);

		}
		autocompletetxtView.setText("");
		edtneck.setText("");
		edtshoulder.setText("");
		edtjine.setText("");
		edtchest.setText("");
		edtstomash.setText("");
		edthip.setText("");
		edtwrist.setText("");
		edtsleeve.setText("");
		edtbicep.setText("");
		edtlength.setText("");
		edtwaist.setText("");
		edtoutseam.setText("");
		edtremark.setText("");
		txtdate.setText("");
		chkFinish.setChecked(false);
		chkReady.setChecked(false);
		imgphoto.setImageResource(R.drawable.photoframe);
		selectedImagePath = null;

	}

	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();

	}

	DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;

            // set selected date into textview
            txtdate.setText(new StringBuilder().append(mmonth + 1)
                    .append("-").append(mday).append("-").append(myear)
                    .append(" "));    

        }
    };
	
	protected Dialog  onCreateDialog(int id) {
        switch (id) {
        case DATE_DIALOG_ID:
            // set date picker as current date
            DatePickerDialog _date =   new DatePickerDialog(this, datePickerListener, myear,mmonth,
                    mday){
                @Override
                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                {   
                    if (year < myear)
                        view.updateDate(myear, mmonth, mday);

                    if (monthOfYear < mmonth && year == myear)
                        view.updateDate(myear, mmonth, mday);

                    if (dayOfMonth < mday && year == myear && monthOfYear == mmonth)
                        view.updateDate(myear, mmonth, mday);

                }
            };
            return _date;
        }
        return null;
    }

    
}
