package com.awitd.tailor;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DisplayAdapter extends BaseAdapter {

	// Declare Variables
	Context mContext;
	LayoutInflater inflater;
	private DBHelper myDBHelper;
	private SQLiteDatabase mydb;
	private AlertDialog.Builder builder;
	private List<Customers> CustomerList = new ArrayList<Customers>();
	private ArrayList<Customers> arraylist;

	String status;

	public DisplayAdapter(Context context, List<Customers> customers_List,
			String status) {

		mContext = context;
		this.CustomerList = customers_List;
		inflater = LayoutInflater.from(mContext);
		this.arraylist = new ArrayList<Customers>();
		this.arraylist.addAll(customers_List);
		this.status = status;
	}

	public class ViewHolder {
		TextView Name;

	}

	@Override
	public int getCount() {
		return CustomerList.size();
	}

	@Override
	public Customers getItem(int position) {
		return CustomerList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup parent) {

		final ViewHolder holder;
		if (view == null) {

			holder = new ViewHolder();
			view = inflater.inflate(R.layout.customer_list, null);
			holder.Name = (TextView) view.findViewById(R.id.txtName);

			view.setTag(holder);
		} else {

			holder = (ViewHolder) view.getTag();
		}
		holder.Name.setText(CustomerList.get(position).getName() + "");

		mydb = (new DBHelper(mContext)).getWritableDatabase();

		view.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				if (status == "R") {
					builder = new AlertDialog.Builder(mContext);
					builder.setTitle("Delete"
							+ CustomerList.get(position).getName());
					builder.setMessage("Do you want to delete?");
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@SuppressWarnings("static-access")
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

									mydb.delete(myDBHelper.TABLE_NAME,
											myDBHelper.KEY_ID
													+ "="
													+ CustomerList
															.get(position)
															.getId(), null);

									dialog.cancel();

									Intent intent = new Intent(mContext,
											DisplayCustomer.class);

									mContext.startActivity(intent);

								}

							});
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.cancel();
								}
							});
					AlertDialog alert = builder.create();
					alert.show();

				}
				return true;
			}

		});
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (status == "R") {

					Intent intent = new Intent(mContext, AddCustomer.class);
					intent.putExtra("ID", CustomerList.get(position).getId());
					intent.putExtra("Name", CustomerList.get(position)
							.getName());
					intent.putExtra("Address", CustomerList.get(position)
							.getAddress());
					intent.putExtra("Phone", CustomerList.get(position)
							.getPhone());
					intent.putExtra("update", true);

					mContext.startActivity(intent);
				}

			}
		});

		return view;
	}

	// Filter Class
	public void filter(String charText) {

		charText = charText.toLowerCase(Locale.getDefault());
		CustomerList.clear();
		if (charText.length() == 0) {
			CustomerList.addAll(arraylist);
		} else {
			for (Customers tp : arraylist) {
				if (tp.getName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					CustomerList.add(tp);
				}
			}
		}
		notifyDataSetChanged();
	}

}
