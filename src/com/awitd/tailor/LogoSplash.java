package com.awitd.tailor;


import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

public class LogoSplash extends Activity {

	private Thread logoTimer;
	public static AssetManager asset;
	public static int displayHeight, displayWidth = 0;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		WindowManager manager = window.getWindowManager();
		Display display = manager.getDefaultDisplay();
		displayWidth = display.getWidth();
		displayHeight = display.getHeight();
		getWindow().addFlags(
				1024 | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		requestWindowFeature(1);
		setContentView(R.layout.logo_splash);
		asset = getAssets();
		logoTimer = new Thread() {

			@Override
			public void run() {

				try {
					synchronized (this) {
						wait(800);
					}

				} catch (InterruptedException e) {
					// TODO: handle exception
				} finally {
					if (!isFinishing()) {
						finish();
						nextfun();

					}
				}

			}

			private void nextfun() {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getBaseContext(), MainActivity.class);
				startActivity(intent);
				overridePendingTransition(android.R.anim.bounce_interpolator,
						android.R.anim.bounce_interpolator);
			}

		};
		logoTimer.start();

		LinearLayout splash = (LinearLayout) findViewById(R.id.splash);
		splash.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (logoTimer.isAlive()) {
					logoTimer.interrupt();
				}

				startActivity(new Intent(LogoSplash.this, MainActivity.class));
				finish();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
		System.exit(0);
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

}
