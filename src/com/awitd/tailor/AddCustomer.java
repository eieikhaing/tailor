package com.awitd.tailor;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint("NewApi")
public class AddCustomer extends Activity {
	private ActionBar actionbar;
	EditText edtName, edtAddress, edtPhone;
	Button btnSave;
	Button btnCall, btnMsg;
	private DBHelper dbHelper;
	private boolean isUpdated;
	int id;
	private String name, address, phone;
	private SQLiteDatabase db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_customer);

		dbHelper = new DBHelper(this);

		actionbar = getActionBar();
		actionbar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#f06292")));
		actionbar.setDisplayShowTitleEnabled(true);
		actionbar.setDisplayHomeAsUpEnabled(true);

		edtName = (EditText) findViewById(R.id.edtName);
		edtAddress = (EditText) findViewById(R.id.edtAddress);
		edtPhone = (EditText) findViewById(R.id.edtPhone);
		btnCall = (Button) findViewById(R.id.btnCall);
		btnCall.setVisibility(View.INVISIBLE);
		btnMsg = (Button) findViewById(R.id.btnMsg);
		btnMsg.setVisibility(View.INVISIBLE);
		btnSave = (Button) findViewById(R.id.btnSave);

		isUpdated = getIntent().getExtras().getBoolean("update");

		if (isUpdated) {
			btnMsg.setVisibility(View.VISIBLE);
			btnCall.setVisibility(View.VISIBLE);
			id = getIntent().getExtras().getInt("ID");
			name = getIntent().getExtras().getString("Name");
			address = getIntent().getExtras().getString("Address");
			phone = getIntent().getExtras().getString("Phone");

			edtName.setText(name);
			edtAddress.setText(address);
			edtPhone.setText(phone);
		}

		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				name = edtName.getText().toString().trim();
				address = edtAddress.getText().toString().trim();
				phone = edtPhone.getText().toString().trim();
				if (name.length() > 0 && address.length() > 0
						&& phone.length() > 0) {
					saveCustomer();
					Toast.makeText(getApplicationContext(),
							"Save the record successfully!", Toast.LENGTH_LONG)
							.show();
				} else {
					AlertDialog.Builder alertBuilder = new AlertDialog.Builder(
							AddCustomer.this);
					alertBuilder.setTitle("Invalid Data");
					alertBuilder
							.setMessage("အမည္၊ လိပ္စာႏွင့္ ဖုန္းနံပါတ္ ျဖည့္ပါ။");
					alertBuilder.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.cancel();
								}

							});
					alertBuilder.create().show();
				}

			}
		});

	}

	private void saveCustomer() {

		db = dbHelper.getWritableDatabase();

		ContentValues value = new ContentValues();
		value.put(DBHelper.KEY_NAME, name);
		value.put(DBHelper.KEY_ADDRESS, address);
		value.put(DBHelper.KEY_PHONE, phone);
		if (isUpdated) {
			db.update(DBHelper.TABLE_NAME, value, DBHelper.KEY_ID + "=" + id,
					null);
			Intent intent = new Intent(this, DisplayCustomer.class);

			startActivity(intent);
		} else {
			db.insert(DBHelper.TABLE_NAME, null, value);
			Intent intent = new Intent(this, DisplayCustomer.class);

			startActivity(intent);
		}
		db.close();
		finish();
	}

	public void Call(View v) {

		String phno = edtPhone.getText().toString();
		Intent phonecall = new Intent(android.content.Intent.ACTION_CALL,
				Uri.parse("tel:" + phno));
		startActivity(phonecall);
	}

	public void sendMessage(View v) {

		String phno = edtPhone.getText().toString();
		Intent sendMsg = new Intent(android.content.Intent.ACTION_SENDTO,
				Uri.parse("sms:" + phno));
		startActivity(sendMsg);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
