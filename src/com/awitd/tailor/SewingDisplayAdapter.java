package com.awitd.tailor;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class SewingDisplayAdapter extends BaseAdapter {

	// Declare Variables
	Context mContext;
	LayoutInflater inflater;
	private DBHelper myDBHelper;
	private SQLiteDatabase mydb;
	private AlertDialog.Builder builder;
	private List<Clothes> ClothesList = new ArrayList<Clothes>();
	private ArrayList<Clothes> arraylist;

	String status;
	String intentpath;

	public SewingDisplayAdapter(Context context, List<Clothes> clothes_List,
			String status) {
		
		mContext = context;
		this.ClothesList = clothes_List;
		inflater = LayoutInflater.from(mContext);
		this.arraylist = new ArrayList<Clothes>();
		this.arraylist.addAll(clothes_List);
		this.status = status;
	}

	public class ViewHolder {
		TextView placeName;
		TextView Date;
	}

	@Override
	public int getCount() {
		return ClothesList.size();
	}

	@Override
	public Clothes getItem(int position) {
		return ClothesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.sewing_clothes_list, null);

			holder.placeName = (TextView) view.findViewById(R.id.txtPName);
			holder.Date = (TextView) view.findViewById(R.id.txtDate);

			view.setTag(holder);

		} else {
			holder = (ViewHolder) view.getTag();
		}

		holder.placeName.setText(ClothesList.get(position).getmName() + "");
		holder.Date.setText(ClothesList.get(position).getmDate() + "");
		mydb = (new DBHelper(mContext)).getWritableDatabase();

		view.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				if (status == "R") {
					builder = new AlertDialog.Builder(mContext);
					builder.setTitle("Delete"
							+ ClothesList.get(position).getmName());
					builder.setMessage("Do you want to delete?");
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@SuppressWarnings("static-access")
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

									mydb.delete(myDBHelper.TABLE_NAME1,myDBHelper.M_ID
													+ "="
													+ ClothesList.get(position)
															.getmId(), null);

									dialog.cancel();
									Intent intent = new Intent(mContext,
											SewingClothesList.class);
									
									mContext.startActivity(intent);

								}

							});
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.cancel();
								}
							});
					AlertDialog alert = builder.create();
					alert.show();

				}
				return true;
			}

		});
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (status == "R") {

					Intent intent = new Intent(mContext, Measurement.class);

					intent.putExtra("ID", ClothesList.get(position).getmId());
					intent.putExtra("Name", ClothesList.get(position)
							.getmName());
					intent.putExtra("Neck", ClothesList.get(position)
							.getmNeck());
					intent.putExtra("Shoulder", ClothesList.get(position)
							.getmShoulder());
					intent.putExtra("Jine", ClothesList.get(position)
							.getmJine());
					intent.putExtra("Chest", ClothesList.get(position)
							.getmChest());
					intent.putExtra("Stomash", ClothesList.get(position)
							.getmStomash());
					intent.putExtra("Hip", ClothesList.get(position).getmHip());
					intent.putExtra("Wrist", ClothesList.get(position)
							.getmWrist());
					intent.putExtra("Sleeve", ClothesList.get(position)
							.getmSleeve());
					intent.putExtra("Bicep", ClothesList.get(position)
							.getmBicep());
					intent.putExtra("Length", ClothesList.get(position)
							.getmLength());
					intent.putExtra("Waist", ClothesList.get(position)
							.getmWaist());
					intent.putExtra("Outseam", ClothesList.get(position)
							.getmOutseam());
					intent.putExtra("Date", ClothesList.get(position)
							.getmDate());
					intent.putExtra("Check", ClothesList.get(position)
							.getmCheck());
					intent.putExtra("Ready", ClothesList.get(position)
							.getmReady());
					intent.putExtra("Remark", ClothesList.get(position)
						.getmRemark());
					intent.putExtra("path", ClothesList.get(position)
							.getmPath());
					intent.putExtra("sewingupdate", true);
					

					mContext.startActivity(intent);
				}

			}
		});

		return view;
	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		ClothesList.clear();
		if (charText.length() == 0) {
			ClothesList.addAll(arraylist);
		} else {
			for (Clothes tp : arraylist) {
				if (tp.getmName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					ClothesList.add(tp);
				}
			}
		}
		notifyDataSetChanged();
	}

}
